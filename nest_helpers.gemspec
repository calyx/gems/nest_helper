$:.push File.expand_path("lib", __dir__)

require "nest_helpers/version"

Gem::Specification.new do |spec|
  spec.name        = "nest_helpers"
  spec.version     = NestHelpers::VERSION
  spec.authors     = ["calyx"]
  spec.email       = ["webdev@calyx.org"]
  spec.homepage    = "https://0xacab.org/calyx/gems/nest_helpers"
  spec.summary     = "Various utilities for Ruby on Rails web applications"
  spec.description = "Various utilities for Ruby on Rails web applications"
  spec.license     = "MIT"

  spec.files = Dir["{app,config,db,lib}/**/*", "LICENSE", "Rakefile", "README.md"]

  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency "warning"
  spec.add_development_dependency "rails", ">= 7.0.7"
end
