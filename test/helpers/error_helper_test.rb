#
# Adapted from rails_autolink gem
#
require "test_helper"

module NestHelpers
  class ErrorHelperTest < ActionView::TestCase
    def test_error_message
      render_error('oh no!')
      assert_select 'p', 'oh no!'
    end
  end
end