module NestHelpers
  module NestHelper
    def display_time_ago(time)
      content_tag :span, class: 'hover', title: time.to_s do
        time_ago_in_words time
      end
    end
  end
end