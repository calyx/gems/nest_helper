module NestHelpers
  module IconHelper
    ICON_SET = :fontawesome
    FA_ICON_MAP = {
      "alert" => "exclamation-triangle",
      "cancel" => "times",
      "charge" => "file-invoice-dollar",
      "complete" => "check-circle",
      "credit" => "tag",
      "destroy" => "trash",
      "device" => "sim-card",
      "device-model" => "pager",
      "erase" => "eraser",
      "failed" => "times-circle",
      "info" => "info",
      "join" => "fist-raised",
      "member" => "user",
      "members" => "users",
      "membership" => "address-card",
      "membership-level" => "poll",
      "new" => "plus",
      "next" => "chevron-right", # should switch for RTL
      "no" => "times-circle",
      "ok" => "check",
      "payment" => "credit-card",
      "pending" => "clock",
      "plus" => "plus",
      "premium" => "gift",
      "prev" => "chevron-left", # should switch for RTL
      "profile" => "user-circle",
      "restore" => "redo-alt",
      "save" => "check",
      "send" => "paper-plane",
      "settings" => "cog",
      "ship" => "shipping-fast",
      "subscription" => "calendar-alt",
      "yes" => "check-circle",
      "reload" => "redo",
      "edit" => "pencil-alt"
    }

    def icon(icon, text=nil)
      if ICON_SET == :fontawesome
        icon = FA_ICON_MAP[icon.to_s] || icon
        str = %(<i class="fa fa-fw fa-#{icon}"></i>).html_safe
      end
      if text
        ("%s %s" % [str, h(text)]).html_safe
      else
        str
      end
    end

    def icon_right(icon, text)
      if ICON_SET == :fontawesome
        icon = FA_ICON_MAP[icon.to_s] || icon
        str = %(<i class="fa fa-fw fa-#{icon}"></i>).html_safe
      end
      ("%s %s" % [h(text), str]).html_safe
    end

    def success_icon
      content_tag(:span, icon("check-circle"), class: "text-success")
    end

    def danger_icon
      content_tag(:span, icon("exclamation-circle"), class: "text-danger")
    end
  end
end