module NestHelpers
  module NavigationHelper
    #
    # returns the string 'active' if certain conditions are met.
    #
    # three forms:
    #
    # * array: active if the controller is any of the ones in the array
    # * hash: active if every condition in the hash is true
    # * array of hashes: each element is handled as a hash.
    #
    def active(*args)
      if args.first.is_a?(Hash)
        args.each do |hsh|
          is_active = hsh.keys.inject(true) {|memo,i|
            memo &&= begin
              if hsh[i].is_a? Regexp
                !!(hsh[i] =~ params[i])
              else
                [hsh[i]].flatten.include?(params[i])
              end
            end
          }
          if is_active
            return 'active'
          end
        end
        return nil
      else
        args.each do |name|
          if name.is_a? Regexp
            return 'active' if params[:controller] =~ name
          else
            return 'active' if params[:controller] == name.to_s
          end
        end
        return nil
      end
    end

    def nav_tab(label, path, klass, disabled: false)
      content_tag('li', class: 'nav-item') do
        link_to(label, path, class: ['nav-link', klass, disabled ? 'disabled' : nil].compact.join(' '))
      end
    end

    def nav_link(active:, path:, label:, icon:, disabled: false)
      if disabled
        content_tag(:span, class: "nav-link disabled #{active}") do
          icon(icon,label)
        end
      else
        link_to( icon(icon, label), path, class: "nav-link #{active}")
      end
    end

  end
end
