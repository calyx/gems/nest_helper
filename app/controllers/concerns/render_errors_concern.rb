module RenderErrorsConcern
  extend ActiveSupport::Concern

  included do
    # to see error pages in development mode, set consider_all_requests_local
    # to `false` in config/environments/development.rb

    unless Rails.application.config.consider_all_requests_local
      rescue_from StandardError,                   with: :render_exception
      rescue_from ActiveRecord::RecordNotFound,    with: :render_not_found
      rescue_from ActionController::RoutingError,  with: :render_not_found
      rescue_from "NotFound",                      with: :render_not_found
      rescue_from ActiveRecord::ConnectionNotEstablished, with: :render_exception
    end
  end

  class_methods do
  end

  protected

  def render_exception(exc)
    if defined?(ErrorMessage) && exc.is_a?(ErrorMessage)
      @message = exc.message
    end
    notify_exception(exc, msg: @message)
    render template: "errors/message", status: 500, layout: "center"
  end

  def render_not_found
    respond_to do |format|
      format.html { render template: "errors/not_found", layout: "center", status: 404 }
      format.all  { head 404 }
    end
  end

  def notify_exception(exc, msg: nil)
    log_exception(exc)
    if defined?(ExceptionNotifier)
      ExceptionNotifier.notify_exception(
        exc, env: request.env, data: { message: msg, user: current_user&.login }
      )
    end
  end

  def log_exception(exc)
    logger.error("EXCEPTION (%s): %s -- %s" % [exc.class, exc.to_s, exc.backtrace[0..10].join(" >>> ")])
  end

  def log_error(msg)
    logger.error("ERROR: %s -- %s" % [msg, caller[1..11].join(" >>> ")])
  end

end
